import os
import sys
import platform
import time
import datetime
import calendar
from util import *

def logAuthentication(line, failure):
    tid = findTid(line)
    timestamp = findTimeStamp(line)
    ip = findSourceIp(line, failure)
    destIp = findDestIp()
    username=findUsername(line,failure)
    profiloUtenza = findProfiloUtenza(ip)
    if(failure == 1):
    	operazione="autenticazione con successo"
    else:
        operazione="autententicazione fallita"
    output = tid + "," + timestamp + "," + ip + "," + destIp + "," + username + "," + profiloUtenza + "," + operazione + ",,"
    return output

def logAuthorization(line, failure):
    tid = findTid(line)
    timestamp = findTimeStamp(line)
    ip = findSourceIpAuthorization(line,failure)
    destIp = findDestIp()
    username=findUsernameAuthorization(line)
    profiloUtenza = findProfiloUtenza(ip)
    if(failure == 1):
    	operazione="autorizzato"
    else:
        operazione="non autorizzato"
    typeevent = findOperation(line)
    region = findRegion(line)
    output = tid + "," + timestamp + "," + ip + "," + destIp + "," + username + "," + profiloUtenza + "," + operazione + "," + typeevent + "," + region
    return output


######################MAIN###################################
def main(argv):
    
    #checking if input folder is specify
    if (len(sys.argv)!=2):
        print("you need to specify input folder")
        sys.exit(-1)

    #create output directory ./logCollector
    createOutputDir()
        
    files = [i for i in os.listdir(sys.argv[1]) if i.endswith("log")]

    #open output file
    outputFile= open(outputfilename,"w+")
    condWrite = False

    for file in files:
	file = sys.argv[1] + file
        # Check file creation date against today
        newFile = checkDate(file)
        # If the file is before yesterday continue
        if (newFile == False):
            continue

        f = open(file)
        lines = f.readlines()
        lines = [i.strip('\n') for i in lines]

        for line in lines:
            newDate = checkDate2(line)
            if(newDate == False):
                continue

            output = None
            #Logged succesfully
            if "succesfully logged to the system" in line:
               output = logAuthentication(line, 1)

            if "WRONG USERNAME AND PASSWORD" in line:
                output = logAuthentication(line, 2)

            if "not approved to operation" in line:
                output = logAuthorization(line, 2)

            elif "approved to operation" in line:
                output = logAuthorization(line, 1)


            if(output != None):
                condWrite = True
                outputFile.write(output + "\n")
                
        f.close()
    if(condWrite == False):
        outputFile.write("###EMTPY###")

        
    outputFile.close()

# start main function
if __name__ == "__main__":
    main(sys.argv)
