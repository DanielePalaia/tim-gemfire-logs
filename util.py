import os
import sys
import platform
import time
import datetime
import calendar
import socket

application= "gemfire."
logtype = "STDAPPL."
count="1."
countLastDay="1."
hostname=socket.gethostname()
timestamp=datetime.datetime.now().strftime('%Y-%m-%d_%H-%M')
outputfilename="./logCollector/"+application+logtype+count+countLastDay+hostname+"."+str(timestamp)+".log"

##############STRING FUNCTIONS############################
#find tid
def findTid(line):
    index = line.find("tid=")
    index2 = line.find("]")
    tid = line[index+4:index2]
    return tid

#find timestamp
def findTimeStamp(line):
    index = line.find(" ")
    index2 = line.find("CEST")
    timestamp = line[index+1:index2+4]
    return timestamp

#find ip
def findSourceIp(line,type):
    if (type==1):
        index = line.find("ip")
        index2 = line.find("succesfully")
    else:
        index = line.find("ip:")
        index2 = line.find("not")
    ip = line[index+4:index2-1]
    return ip

def findSourceIpAuthorization(line, type):
    if (type==1):
        index = line.find("ip")
        index2 = line.find("approved")
    else:
        index = line.find("ip:")
        index2 = line.find("not approved")
    ip = line[index+4:index2-1]
    return ip

def findDestIp():
    return "localhost"

def findUsername(line, type):
    if(type == 1):
        index = line.find("User:")
        index2 = line.find("with")
        User = line[index+6:index2-1]
    else:
        index = line.find("User:")
        index2 = line.find("from")
        User = line[index+6:index2-1]
    return User

def findUsernameAuthorization(line):
    index = line.find("user")
    index2 = line.find("with")
    User = line[index+5:index2-1]
    return User

def findProfiloUtenza(ip):
    if ("null" in ip):
        return "Nominale"
    else:
        return "M2M"

def findOperation(line):
    index = line.find("operation")
    index2 = line.find("on accessing")
    operation = line[index+10:index2-1]
    return operation

def findRegion(line):
    index = line.find("on region")
    index2 = line.find("\n")
    operation = line[index+10:index2]
    return operation

def creation_date(path_to_file):

    #return datetime.datetime.strptime(os.path.getctime(path_to_file), "%a %b %d %H:%M:%S %Y")
    return time.ctime(os.path.getctime(path_to_file))

def checkDate(file):
    ######Checking creation date
    cdate = creation_date(file)
    now=datetime.datetime.now()
    cdate = datetime.datetime.strptime(cdate, "%a %b %d %H:%M:%S %Y")
    if((now - cdate).days > 1):
        return False
    return True

def findTimeStampDay(timestamp):
    timestamp2 = timestamp[:10]
    date=None
    try:
    	if(timestamp!=''):
    		date = datetime.datetime.strptime(timestamp2, "%Y/%m/%d")
    except Exception,e:
        return None

    return date

def checkDate2(line):
    # skip old (before yesterday) timestamp
    timestamp = findTimeStamp(line)
    dayTimeStamp = findTimeStampDay(timestamp)
    if(dayTimeStamp == None):
	return False
    now=datetime.datetime.now()
    
    if((now - dayTimeStamp).days == 1):
        return True
    
    return False

def createOutputDir():
    if not os.path.exists("./logCollector"):
        os.makedirs("./logCollector")
